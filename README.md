# Ansible Gitlab Runner

Example how to install and register a gitlab runner using ansible providing misc. values like S3 cache server, Docker registry auth token,...

* Settings and variables can be found in `roles/gitlab-runner/vars/main.yml`
* Use vaults for `roles/gitlab-runner/vars/main.yml`!

## Install and register

### Docker

    ansible-playbook -e target=ubuntu-runner-2 -e executor=docker ~/ansible/playbooks/gitlab-runner/install.yml --ask-vault-pass

### Shell

    ansible-playbook -e target=ubuntu-runner-2 -e executor=shell ~/ansible/playbooks/gitlab-runner/install.yml --ask-vault-pass
